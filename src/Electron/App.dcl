definition module Electron.App

/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from StdOverloaded import class ==, class <

from ABC.Interpreter.JavaScript import :: JSVal, :: JSWorld
from ABC.Interpreter.JavaScript.Monad import :: JS

from Data.GenEq import generic gEq
from Data.GenHash import generic gHash
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Engine import :: EngineOptions, :: StartableTask
from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.SDS.Definition import :: SimpleSDSLens, :: SDSLens
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import :: Task, class iTask

:: ElectronOptions =
	{ port  :: !Int
	, debug :: !Bool
	}

:: ElectronWindow =: ElectronWindow Int

:: ElectronWindowOptions a =
	{ task           :: !(ElectronWindow -> Task a) //* The task to evaluate
	, webPreferences :: ![(String, JSVal)]          //* A list of `webPreferences` passed to `BrowserWindow`
	, windowModifier :: !JSVal -> JS () JSVal       //* A function to modify the electron `BrowserWindow`
	, parent         :: !?ElectronWindow            //* For modal windows
	, width          :: !Int                        //* A suggestion for the width, in pixels
	, height         :: !Int                        //* A suggestion for the height, in pixels
	}

derive class iTask ElectronWindow
derive gHash ElectronWindow

instance == ElectronWindow
instance < ElectronWindow

tcpQueueEmpty :: SimpleSDSLens Bool

serveElectron ::
	!([String] ElectronOptions EngineOptions -> (ElectronOptions, EngineOptions, [StartableTask]))
	!(Task a)
	!*World -> *World
	| iTask a

defaultOptions :: ElectronWindowOptions a

allWindows :: SimpleSDSLens [ElectronWindow]
currentWindow :: SimpleSDSLens (?ElectronWindow)

createWindow :: !(ElectronWindowOptions a) -> Task ElectronWindow | iTask a

/**
 * Get the Electron `BrowserWindow` corresponding to an `ElectronWindow`.
 * This function is meant to be run in the main process, i.e. in
 * `runInMainProcess` or `runInMainProcessWithResult`.
 */
getBrowserWindow :: !ElectronWindow !*JSWorld -> (!?JSVal, !*JSWorld)

closeWindow :: Task ()

closeAllWindows :: Task ()

loadURL :: String -> Task ()

/**
 * Run a client-side function in the main electron process. The task becomes
 * stable immediately.
 */
runInMainProcess :: !(JSVal *JSWorld -> *JSWorld) -> Task ()

/**
 * Run a client-side function in the main electron process. The task waits for
 * the function to return and becomes stable with its return value.
 */
runInMainProcessWithResult :: !(JSVal *JSWorld -> *(a, *JSWorld)) -> Task a | iTask a
